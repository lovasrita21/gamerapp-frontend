import {InvitedPlayerIdsAndStatuses} from "./invitedPlayerIdsAndStatuses";
import {TrainingType} from "./trainingType";

export interface Training {
    date: Date,
    id:	string,
    info: string,
    invitedPlayerIdsAndStatuses: Map<string, InvitedPlayerIdsAndStatuses>,
    teamId:	string,
    trainingType: TrainingType
}
