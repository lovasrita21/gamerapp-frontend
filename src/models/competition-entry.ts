export interface CompetitionEntry {
    broadcastLink:	string,
    date:	string,
    id:	string,
    info:	string,
    link:	string,
    location:	string,
    memberIds:	Array<string>,
    name:	string,
    teamId: string
}
