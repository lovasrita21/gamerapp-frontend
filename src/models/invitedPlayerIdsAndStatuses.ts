export enum InvitedPlayerIdsAndStatuses {
    ACCEPTED = "ACCEPTED",
    PENDING = "PENDING",
    REJECTED = "REJECTED"
}
