import {Game} from "./game";

export interface PlayerGameDetail {
    game: Game,
    ign: string,
    teamId: string,
    teamRole: string
    //
    // gameId: string,
    // id: string,
    // nickName: string,
    //
}
