export interface ImageDTO {

    fileDownloadUri: string,
    fileName: string,
    fileType: string,
    size: number
}


