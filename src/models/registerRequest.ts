export interface RegisterRequest {
  firstName: string,
  lastName: string,
  isLastNameHidden: boolean,
  username: string,
  email: string,
  password: string,
  aboutMe: string,
  gender: string
}
