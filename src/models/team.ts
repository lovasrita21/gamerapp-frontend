export interface Team {
    id: string,
    leaderId: string,
    lostCompetitionIds: [ string ],
    membersId: [ string ],
    name: string,
    wonCompetitionIds: [ string ]
}
