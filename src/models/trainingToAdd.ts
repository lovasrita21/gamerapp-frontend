import {TrainingType} from "./trainingType";

export interface TrainingToAdd {
    date: Date,
    id:	string,
    info: string,
    invitedPlayerIds: Array<string>;
    teamId:	string,
    trainingType: TrainingType
}
