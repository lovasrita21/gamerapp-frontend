import {Role} from "./role";
import {Team} from "./team";
import { PlayerGameDetail } from "./playerGameDetail";

export interface Profile {
    birthday: string,
    email: string,
    firstName: string,
    id: string,
    lastName: string,
    password: string,
    profilePic: string,
    roles: [ Role ],
    teams: [ Team ],
    username: string,
    gameDetails: [ PlayerGameDetail ]
}

