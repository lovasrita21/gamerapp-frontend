export interface News {
    id: string,
    authorId: string,
    authorName: string,
    authorPictureUrl: string,
    created: string,
    newsType: string,
    gameId: string,
    teamId: string,
    title: string,
    content: string,
    pictureUrls: [string]
}
