import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Profile} from "../../../models/profile";
import {TrainingType} from "../../../models/trainingType";
import {ProfileProvider} from "../../../providers/profile/profile";
import {CompetitionEntryProvider} from "../../../providers/competition-entry/competition-entry";
import {CompetitionEntry} from "../../../models/competition-entry";

/**
 * Generated class for the AddCompetitionEntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-competition-entry',
  templateUrl: 'add-competition-entry.html',
})
export class AddCompetitionEntryPage {

  newCompetitionEntry: CompetitionEntry | any = {};

  myProfile: Profile | any;
  selectTeamList: Array<string> = [];
  selectedTeam: string = null;
  selectedType: TrainingType;
  info: string = "";
  link: string = "";
  bclink: string = "";
  name: string = "";
  location: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, private competitionEntryProvider: CompetitionEntryProvider, private profileProvider: ProfileProvider) {
  }

  async ionViewDidLoad() {
    this.myProfile = await this.profileProvider.getMyProfile();
    for(let team of this.myProfile.teams){
      this.selectTeamList.push(team.name);
    }
  }

  addCompetitionEntry() {
    this.newCompetitionEntry.date = new Date();

    for(let team of this.myProfile.teams) {
      if(team.name === this.selectedTeam) {
        this.newCompetitionEntry.teamId = team.id;
      }
    }

    this.newCompetitionEntry.memberIds = ["123", "234"];

    this.newCompetitionEntry.info = this.info;
    this.newCompetitionEntry.broadcastLink = this.bclink;
    this.newCompetitionEntry.link = this.link;
    this.newCompetitionEntry.location = this.location;
    this.newCompetitionEntry.name = this.name;
    console.log(this.newCompetitionEntry);
    this.competitionEntryProvider.addCompetitionEntry(this.newCompetitionEntry);
    this.navCtrl.pop();
  }

}
