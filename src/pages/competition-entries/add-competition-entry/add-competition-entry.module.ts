import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCompetitionEntryPage } from './add-competition-entry';

@NgModule({
  declarations: [
    AddCompetitionEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(AddCompetitionEntryPage),
  ],
})
export class AddCompetitionEntryPageModule {}
