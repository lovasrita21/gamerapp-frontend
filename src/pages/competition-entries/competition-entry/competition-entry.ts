import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Profile} from "../../../models/profile";
import {ProfileProvider} from "../../../providers/profile/profile";
import {CompetitionEntry} from "../../../models/competition-entry";
import {CompetitionEntryProvider} from "../../../providers/competition-entry/competition-entry";

/**
 * Generated class for the CompetitionEntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-competition-entry',
  templateUrl: 'competition-entry.html',
})
export class CompetitionEntryPage {

  allCompetitions: Array<CompetitionEntry> |any;
  competitions: Array<CompetitionEntry>;
  myProfile: Profile | any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private competitionEntryProvider: CompetitionEntryProvider, private profileProvider: ProfileProvider) {
  }
  async ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingPage');
    this.getMyprofile();
  }
  async getMyprofile() {
    this.myProfile = await this.profileProvider.getMyProfile()
  }

  async ionViewWillEnter() {
    this.getCompetitions();
  }

  async getCompetitions() {
    try {
      this.allCompetitions = await this.competitionEntryProvider.getCompetitionsById(this.myProfile.id);
      this.competitions = this.allCompetitions;
    } catch (e) {
      console.log(e)
    }
  }

  capitalize(string) {
    if(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    } else {
      return "";
    }
  }
  maxLength(string) {
    return string.substring(0,20)+"...";
  }

  setDate(p_date) {
    let date = new Date(p_date);
    return date.getFullYear() + ". " + date.getMonth() + ". " + date.getDate() + ".";
  }

  getNameOfTeam(id) {
    for( let team of this.myProfile.teams) {
      if(team.id === id) {
        return team.name;
      }
    }
    return "Hiba: a csapat nem létezik"
  }
  newCompetitionEntry() {
    this.navCtrl.push('AddCompetitionEntryPage');
  }

}
