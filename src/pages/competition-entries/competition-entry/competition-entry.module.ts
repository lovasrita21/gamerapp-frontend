import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompetitionEntryPage } from './competition-entry';

@NgModule({
  declarations: [
    CompetitionEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(CompetitionEntryPage),
  ],
})
export class CompetitionEntryPageModule {}
