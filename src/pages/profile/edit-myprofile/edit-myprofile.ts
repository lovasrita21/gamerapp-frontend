import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProfileProvider} from "../../../providers/profile/profile";
import {Profile} from "../../../models/profile";
import {Game} from "../../../models/game";

/**
 * Generated class for the EditMyprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-myprofile',
  templateUrl: 'edit-myprofile.html',
})
export class EditMyprofilePage {

  myProfile: Profile | any = {};
  firstname: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private profileProvider: ProfileProvider) {
  }

  async ionViewDidLoad() {
    this.getMyprofile();
    this.firstname = this.myProfile.firstName;
  }

  async getMyprofile() {
    this.myProfile = await this.profileProvider.getMyProfile()
  }

  hasTeamInGame(game: Game) {
    for (let team of this.myProfile.teams) {
      if (team.gameId === game.id) {
        return true;
      }
    }
    return false;
  }

  getTeamOfGame(game: Game) {
    for (let team of this.myProfile.teams) {
      if (team.gameId === game.id) {
        return team.name;
      }
    }
  }
  updateProfile() {
    this.profileProvider.updateMyProfile(this.myProfile);
    this.navCtrl.pop();
  }
}
