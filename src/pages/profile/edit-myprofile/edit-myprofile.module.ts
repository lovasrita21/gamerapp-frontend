import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditMyprofilePage } from './edit-myprofile';

@NgModule({
  declarations: [
    EditMyprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(EditMyprofilePage),
  ],
})
export class EditMyprofilePageModule {}
