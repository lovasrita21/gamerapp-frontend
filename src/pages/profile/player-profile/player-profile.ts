import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProfileProvider} from "../../../providers/profile/profile";
import {Profile} from "../../../models/profile";
import {Game} from "../../../models/game";

/**
 * Generated class for the MyprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-player-profile',
  templateUrl: 'player-profile.html',
})
export class PlayerProfilePage {

  profile: Profile | any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, private profileProvider: ProfileProvider) {
  }

  async ionViewDidLoad() {
    let id = this.navParams.get('id');
    console.log("EZ AZ ID: " + id);
    this.profile = await this.profileProvider.getProfile(id);
  }

  hasTeamInGame(game: Game) {
    for (let team of this.profile.teams) {
      if (team.gameId === game.id) {
        return true;
      }
    }
    return false;
  }

  getTeamOfGame(game: Game) {
    for (let team of this.profile.teams) {
      if (team.gameId === game.id) {
        return team.name;
      }
    }
  }
}
