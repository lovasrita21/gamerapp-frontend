import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User } from '../../../providers';
import { MainPage } from '../../index';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import {RegisterRequest} from "../../../models/registerRequest";


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: './signup.html'
})
export class SignupPage {
  account: RegisterRequest = {
    email: 'palmarcell96@gmail.com',
    firstName: 'Marcell',
    lastName: 'Pál',
    isLastNameHidden: true,
    password: 'kaka',
    username: 'gemboly',
    aboutMe: 'Szeretem a pizzát.',
    gender: 'man'
  }

  registerFormGroup: FormGroup

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
              public user: User,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              public formBuilder: FormBuilder) {
    this.initForm()
    this.getTranslation()
  }

  getTranslation () {
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  // __TODO__ password match fix
  initForm () {
    let group = {
      email: new FormControl('', [Validators.required, Validators.email]),
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.minLength(3), Validators.required]),
      firstName: new FormControl('', [Validators.required, Validators.minLength(3)]),
      lastName: new FormControl('', [Validators.required, Validators.minLength(3)]),
      isLastNameHidden: new FormControl(),
      aboutMe: new FormControl('', [Validators.required, Validators.minLength(10)]),
      gender: new FormControl('', [Validators.required]),
      gdpr: new FormControl('false', [Validators.requiredTrue])
    }
    this.registerFormGroup = this.formBuilder.group({
      ...group,
      repassword: new FormControl('', [Validators.required, (control) => this.PasswordMatch(control, group.password)])
    })
  }

  PasswordMatch (control: AbstractControl, passwordControl: AbstractControl) {
    if (control.value !== passwordControl.value) {
      return {passwordMismatch: true}
    }
  }

  async doSignup() {
    try {
      if(!this.registerFormGroup.valid) {
        throw Error('Nem valid a cucc')
      }
      let response = await this.user.signup(this.account)
      this.navCtrl.push(MainPage);
      console.log({response})
    } catch (e) {
      let toast = this.toastCtrl.create({
        message: this.signupErrorString + ': ' + e.message,
        duration: 3000,
        position: 'top'
      })
      toast.present();
    }
  }
}
