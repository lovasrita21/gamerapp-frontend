import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User } from '../../providers';
import { MainPage } from '../index';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  account: { usernameOrEmail: string, password: string } = {
    usernameOrEmail: 'admin',
    password: 'krumpli'
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  // Attempt to login in through our User service
  async doLogin() {

    try {
      let response = await this.user.login(this.account)
      this.navCtrl.push(MainPage)
      console.log("Ez a login response: "+ {response});
    } catch (e) {
      console.log({e})
      let toast = this.toastCtrl.create({
        message: this.loginErrorString + ': ' + e.message,
        duration: 3000,
        position: 'top'
      })
      toast.present();
    }
  }
}
