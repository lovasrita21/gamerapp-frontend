import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewseditorPage } from './newseditor';

@NgModule({
  declarations: [
    NewseditorPage,
  ],
  imports: [
    IonicPageModule.forChild(NewseditorPage),
  ],
  exports: [
    NewseditorPage
  ]
})
export class NewseditorPageModule {}
