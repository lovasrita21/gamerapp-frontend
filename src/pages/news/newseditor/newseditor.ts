import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {NewsProvider} from "../../../providers/news/news";
import {News} from "../../../models/news";
import {Profile} from "../../../models/profile"
import {ProfileProvider} from "../../../providers/profile/profile";

/**
 * Generated class for the NewseditorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-newseditor',
    templateUrl: 'newseditor.html',
})
export class NewseditorPage {

    myProfile: Profile | any = {id: ""};
    private newsToSend: News | any = {};

    titleToSend: string;
    contentToSend: string;
    selectTypeList: Array<string> = [];
    selectGameList: Array<string> = [];
    selectTeamList: Array<string> = [];

    selectedType: string = null;
    selectedGame: string = null;
    selectedTeam: string = null;

    response_text: string = "default text";

        constructor( public navCtrl: NavController, public navParams: NavParams, private news: NewsProvider, private profileProvider: ProfileProvider) {
    }

    async ionViewDidLoad() {
        this.myProfile = await this.profileProvider.getMyProfile()
        let roles = ['ROLE_USER', 'ROLE_TEAM_LEADER', 'ROLE_MIDDLE_LEADER', 'ROLE_PRO_LEADER', 'ROLE_ADMIN']

        this.selectTypeList.push('Csapat','Játék')
        if( this.myProfile.roles.roleName === roles[1]){
            this.selectTypeList.push('Csapatvezetői');
        } else if(this.myProfile.roles.roleName === roles[2]) {
            this.selectTypeList.push('Középvezetői')
        } else {
            this.selectTypeList.push('Szakmaivezetőségi')
        }
        this.selectTypeList.push('Közösségi');


        for(let game of this.myProfile.gameDetails) {
           this.selectGameList.push(game.game.name)
        }
        for(let team of this.myProfile.teams){
            this.selectTeamList.push(team.name)
        }

    }

    async submitNews() {

        this.newsToSend.gameId = null;
        this.newsToSend.teamId = null;

        if(this.selectedType === 'Játék' || this.selectedType === 'Csapat') {
            for(let game of this.myProfile.gameDetails) {
                if(game.game.name === this.selectedGame) {
                    this.newsToSend.gameId = game.game.id
                }
            }
            if(this.selectedType === 'Csapat') {
                for(let team of this.myProfile.teams) {
                    if(team.name === this.selectedTeam) {
                        this.newsToSend.teamId = team.id;
                    }
                }
            }
        }

        if(this.selectedType === 'Szakmaivezetőségi') this.newsToSend.newsType = "PRO_LEADERS";
        if(this.selectedType === 'Közösségi') this.newsToSend.newsType = "GLOBAL";
        if(this.selectedType === 'Középvezetői') this.newsToSend.newsType = "MIDDLE_LEADERS";
        if(this.selectedType === 'Csapatvezetői') this.newsToSend.newsType = "TEAM_LEADERS";
        if(this.selectedType === 'Játék') this.newsToSend.newsType = "GAME";
        if(this.selectedType === 'Csapat') this.newsToSend.newsType = "TEAM";

        //nem kell
        this.newsToSend.id = "";
        this.newsToSend.authorId = this.myProfile.id;
        //nem kell
        this.newsToSend.authorName = "";
        this.newsToSend.authorPictureUrl = "";
        //nem kell
        this.newsToSend.created = "";
        this.newsToSend.title = this.titleToSend;
        this.newsToSend.content = this.contentToSend;
        this.newsToSend.pictureUrls = [];

        try {
            console.log(this.newsToSend);
            const response = await this.news.submitNews(this.newsToSend);
            console.log(response);
            this.navCtrl.pop()
        } catch (e) {
            console.log(e)
        }
    }

}
