import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {NewsProvider} from "../../../providers/news/news";
import {News} from "../../../models/news";
import {ProfileProvider} from "../../../providers/profile/profile";
import {Profile} from "../../../models/profile";
//import {SplashScreen} from "@ionic-native/splash-screen";
import { LocalNotifications } from '@ionic-native/local-notifications';

//import deleteProperty = Reflect.deleteProperty;

/**
 * Generated class for the NewsfeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-newsfeed',
  templateUrl: 'newsfeed.html',
})
export class NewsfeedPage {
  myProfile: Profile | any = {id: "", profilePic: ""};

  private allNews: Array<News> | any;
  selectNewsTypes: Array<string> = ['Közérdekű', 'Szakmaivezetőségi', 'Középvezetői', 'Csapatvezetői', 'Játék', 'Csapat'];
  selectedNewsTypes: Array<string> = [];
  selectedNews: Array<News> = [];

  selectGameList: Array<string> = [];
  selectTeamList: Array<string> = [];

  selectedGames: Array<string> = [];
  selectedTeams: Array<string> = [];
  imageSrc: string;

  constructor(public platform: Platform, private localNotifications: LocalNotifications, public navCtrl: NavController, public navParams: NavParams, private newsProvider: NewsProvider, private profileProvider: ProfileProvider) {
  }

  localnotificationHandler() {
    let notification: any = {
      id:0,
      text: 'One Time Notification',
      led: 'FF0000',
    };
    this.localNotifications.schedule (notification);
    this.localNotifications.isScheduled (0);
  }

  async ionViewWillEnter() {
    this.getNews()
  }

  async ionViewDidLoad() {
    this.myProfile = await this.profileProvider.getMyProfile();
    //this.imageSrc = await this.profileProvider.getPictureByProfilePicUrl(this.myProfile.profilePic);
    console.log(this.myProfile);
    for (let game of this.myProfile.gameDetails) {
      console.log(game.game.name)
      this.selectGameList.push(game.game.name)
    }
  }

  sortNews() {
    let tomb: Array<News> = [];
    for (let news of this.selectedNews) {
      if (news.newsType === 'GAME' || news.newsType === 'TEAM')
        tomb.push(news)
    }
    let hasGameType = false;
    let hasTeamType = false;
    for (let newsType of this.selectedNewsTypes) {
      if (newsType === 'Közérdekű') {
        for (let news of this.allNews) {
          if (news.newsType === 'GLOBAL') {
            tomb.push(news);
          }
        }
      }
      if (newsType === 'Szakmaivezetőségi') {
        for (let news of this.allNews) {
          if (news.newsType === 'PRO_LEADERS') {
            tomb.push(news);
          }
        }
      }
      if (newsType === 'Középvezetői') {
        for (let news of this.allNews) {
          if (news.newsType === 'MIDDLE_LEADERS') {
            tomb.push(news);
          }
        }
      }
      if (newsType === 'Csapatvezetői') {
        for (let news of this.allNews) {
          if (news.newsType === 'TEAM_LEADERS') {
            tomb.push(news);
          }
        }
      }
      if(newsType === 'Játék') {
        hasGameType = true;
      }
      if(newsType === 'Csapat') {
        hasTeamType = true;
      }
    }

    this.selectedNews = tomb;

    if(!hasGameType) {
      this.deleteGameNews()
    } else {
      this.sortNewsByGame()
    }
    if(!hasTeamType) {
      this.deleteTeamNews()
    } else {
      this.sortNewsByTeam()
    }
  }
  deleteGameNews() {
    let tomb: Array<News> = [];
    for (let news of this.selectedNews) {
      if (news.newsType !== 'GAME' && news.newsType !== 'TEAM')
        tomb.push(news)
    }
    this.selectedNews = tomb;
  }
  deleteTeamNews() {
    let tomb: Array<News> = []
    for (let news of this.selectedNews) {
      if (news.newsType !== 'TEAM')
        tomb.push(news)
    }
    this.selectedNews = tomb;
  }
  sortNewsByGame() {
    this.deleteGameNews()
    let tomb: Array<News> = [];
    for (let news of this.selectedNews) {
      if (news.newsType !== 'GAME' && news.newsType !== 'TEAM')
        tomb.push(news)
    }
    while (this.selectedTeams.length > 0) {
      this.selectedTeams.pop()
    }
    for (let type of this.selectedNewsTypes) {
      if (type === 'Játék') {
        for (let news of this.allNews) {
          if (news.newsType === 'GAME') {
            for (let game of this.myProfile.gameDetails) {
              if (news.gameId === game.game.id) {
                for (let gamename of this.selectedGames) {
                  if (gamename === game.game.name) {
                    tomb.push(news);
                  }
                }
              }
            }
          }
        }
      }
      if (type === 'Csapat') {
        this.setTeamList();
      }
    }
    this.selectedNews = tomb;

  }
  setTeamList() {
    while(this.selectTeamList.length > 0){
      this.selectTeamList.pop();
    }

    //végig megyek az összes játékán a profilnak
    for (let game of this.myProfile.gameDetails) {
      //minden alkalommal végig megyek a kiválasztott játékokon is
      for (let gamename of this.selectedGames) {
        //ha egyeznek a nevek
        if (gamename === game.game.name) {
          //végig megyek az összes csapatán a profilnak
          for(let team of this.myProfile.teams) {
            //hogy megtaláljam azt, amelyiknek gameid-ja megegyezik a kiválaszott játék id-jával
            if(team.gameId === game.game.id) {
              this.selectTeamList.push(team.name);
            }
          }
        }
      }
    }
  }
  sortNewsByTeam() {
   this.deleteTeamNews()
    let tomb: Array<News> = []
    for (let news of this.selectedNews) {
      if (news.newsType !== 'TEAM')
        tomb.push(news)
    }
    this.setTeamList()
    for (let news of this.allNews) {
      if (news.newsType === 'TEAM') {
        for (let team of this.myProfile.teams) {
          if (news.teamId === team.id) {
            for (let teamname of this.selectedTeams) {
              if (teamname === team.name) {
                tomb.push(news);
              }
            }
          }
        }
      }
    }
    this.selectedNews = tomb;
  }

  async getMyProfile() {
    try {
      const response = await this.profileProvider.getMyProfile()
      console.log(response)
    } catch (e) {
      console.log(e)
    }
  }
  setDate(p_date) {
    let date = new Date(p_date);
    return date.getFullYear() + ". " + date.getMonth() + ". " + date.getDate() + ".";
  }

  async getNews() {
    try {
      this.allNews = await this.newsProvider.getAllNews()
    } catch (e) {
      console.log(e)
    }
  }

  addNews() {
    this.navCtrl.push('NewseditorPage')
  }

  goToMyProfile() {
    this.navCtrl.push('MyprofilePage')
  }
  goToPlayerProfile(id) {
    this.navCtrl.push('PlayerProfilePage', {id: id})
  }

}
