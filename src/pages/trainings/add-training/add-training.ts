import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {TrainingProvider} from "../../../providers/training/training";
import {TrainingToAdd} from "../../../models/trainingToAdd";
import {ProfileProvider} from "../../../providers/profile/profile";
import {Profile} from "../../../models/profile";
import {TrainingType} from "../../../models/trainingType";

/**
 * Generated class for the AddTrainingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-training',
  templateUrl: 'add-training.html',
})
export class AddTrainingPage {
  newtraining: TrainingToAdd | any = {};

  myProfile: Profile | any;
  selectTeamList: Array<string> = [];
  selectedTeam: string = null;
  selectedType: TrainingType;
  info: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, private trainingProvider: TrainingProvider, private profileProvider: ProfileProvider) {
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad AddTrainingPage');
    this.myProfile = await this.profileProvider.getMyProfile();
    for(let team of this.myProfile.teams){
      this.selectTeamList.push(team.name);
    }
  }

  addTraining() {
    this.newtraining.date = new Date();
    for(let team of this.myProfile.teams) {
      if(team.name === this.selectedTeam) {
        this.newtraining.teamId = team.id;
      }
    }

    this.newtraining.invitedPlayerIds = ["123"];

    if(this.selectedType === "ONLINE") {
      this.newtraining.trainingType = TrainingType.ONLINE;
    }else {
      this.newtraining.trainingType = TrainingType.OFFLINE;
    }
    this.newtraining.info = this.info;
    console.log(this.newtraining);
    this.trainingProvider.addTraining(this.newtraining);
    this.navCtrl.pop();
  }


}
