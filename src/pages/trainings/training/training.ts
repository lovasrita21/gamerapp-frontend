import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TrainingProvider } from "../../../providers/training/training";
import { Training } from "../../../models/training";
import {ProfileProvider} from "../../../providers/profile/profile";
import {Profile} from "../../../models/profile";


/**
 * Generated class for the TrainingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-training',
  templateUrl: 'training.html',
})
export class TrainingPage {

  allTraining: Array<Training> |any;
  trainings: Array<Training> = [];
  myProfile: Profile | any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private trainingProvider: TrainingProvider, private profileProvider: ProfileProvider) {
  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad TrainingPage');
    this.getMyprofile();
  }

  async getMyprofile() {
    this.myProfile = await this.profileProvider.getMyProfile()
  }

  async ionViewWillEnter() {
    this.getTrainings();
  }

  async getTrainings() {
    try {
      this.allTraining = await this.trainingProvider.getMyTrainings();
      this.trainings = this.allTraining;
    } catch (e) {
      console.log(e);
    }
  }

  capitalize(string) {
    if(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    } else {
      return "";
    }
  }

  setDate(p_date) {
    let date = new Date(p_date);
    return date.getFullYear() + ". " + date.getMonth() + ". " + date.getDate() + ".";
  }

  getStatus(training: Training) {
    let id = this.myProfile.id;
    return training.invitedPlayerIdsAndStatuses[id];
  }

  setStatus(training: Training, status) {
    if(this.trainingProvider.changeStatus(training.id, status)) {
      training.invitedPlayerIdsAndStatuses[this.myProfile.id] = status;
    }
  }
  getNameOfTeam(id) {
    for( let team of this.myProfile.teams) {
      if(team.id === id) {
        return team.name;
      }
    }
    return "Hiba: a csapat nem létezik"
  }
  newTraining() {
    this.navCtrl.push('AddTrainingPage');
  }
}
