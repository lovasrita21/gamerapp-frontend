import { Injectable } from '@angular/core';
import {Api} from "..";
import {Profile} from "../../models/profile";

/*
  Generated class for the ProfileProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProfileProvider {

  private myProfile: Profile

  constructor(private api: Api) {
  }

  async getMyProfile() {
    try {
      if (!this.myProfile) {
        this.myProfile = await this.api.get('/players/me').toPromise() as Profile
      }
      return this.myProfile
    } catch(e) {
      console.log(e);
    }
  }

  getMyName() {
    return this.myProfile.lastName + " " + this.myProfile.firstName
  }

  async getProfile(id) {
    let response = await this.api.get('/players/'+id).toPromise() as Profile;
    return response;
  }

  async getPictureByProfilePicUrl(fileName: string) {
    //console.log(this.myProfile.id)
    //let response =  await this.api.get('/images/profiles/'+fileName).toPromise() as JSON;
    return ""; //"data:image/jpeg;base64," + response;

  }

  async updateMyProfile(profile: Profile) {
    let response = await this.api.post('/players/update', profile).toPromise();
    return response;
  }
}
