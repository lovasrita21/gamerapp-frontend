import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Api} from "..";
import {TrainingToAdd} from "../../models/trainingToAdd";

/*
  Generated class for the TrainingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TrainingProvider {

    constructor(public http: HttpClient, private api: Api) {
        //console.log('Hello TrainingProvider Provider');
    }

    async getMyTrainings() {
        return await this.api.get("trainings/getMyTrainings").toPromise();
    }

    async addTraining(training: TrainingToAdd) {
        let response = await this.api.post("/trainings/add", training).toPromise();
        console.log(response);
        return response;
    }

    async changeStatus(trainingId: string, status: string) {
        let response = await this.api.get("/trainings/changeStatus/" + trainingId + "/" + status).toPromise();
        console.log(response);
        return response;
    }
}
