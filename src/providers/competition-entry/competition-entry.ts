import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Api} from "..";
import {ProfileProvider} from "../profile/profile";
import {CompetitionEntry} from "../../models/competition-entry";
import {Profile} from "../../models/profile";

@Injectable()
export class CompetitionEntryProvider {

    myProfile: Profile | any;
    constructor(public http: HttpClient, private api: Api, private profileProvider: ProfileProvider) {
        //console.log('Hello TrainingProvider Provider');
    }

    async getCompetitionsById(id: string) {
        return await this.api.get("/entries/"+id).toPromise();
    }

    async addCompetitionEntry(competition: CompetitionEntry) {
        let response = await this.api.post("/entries/add", competition).toPromise();
        console.log(response);
        return response;
    }

}
