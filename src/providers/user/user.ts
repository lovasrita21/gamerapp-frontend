import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  _userData: any;

  constructor(public api: Api) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  async login(accountInfo: any) {
    let response = await this.api.post('/login', accountInfo).toPromise()
    this.loggedIn(response)
    return response
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  async signup(accountInfo: any) {
    let response = await this.api.post('/register', accountInfo).toPromise()
    this.loggedIn(response)
    return response
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._userData = null;
  }

  /**
   * Process a login/signup response to store user data
   */
  private loggedIn(resp) {
    this._userData = resp
    if(!resp.token) {
      throw Error('No token granted from server') // __TODO__ lefordítani
    }
    this.api.setAuth(resp)
  }
}
