import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
  // url: string = 'http://localhost:8080/api';
@Injectable()
export class Api {
  url: string = 'https://deac-hackers-rest.herokuapp.com/api';
  auth: any  = {
    token: ''
  };
  constructor(public http: HttpClient) {

  }

  formatEndPoint(endpoint: string) {
    return endpoint.startsWith('/') ? this.url + endpoint : this.url + '/' + endpoint
  }

  setAuth(auth) {
    this.auth = auth
  }

  getAuth() {
    return this.auth
  }


  // __TODO__ mindhez hozzáadni az auth-ot



  get(endpoint: string, params?: any, reqOpts?: any) {
    let wellFormedEndpoint = this.formatEndPoint(endpoint);
    let headers = new HttpHeaders().set('Authorization', `Bearer ${this.auth.token}`);
    return this.http.get(wellFormedEndpoint, {headers});
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    let wellFormedEndpoint = this.formatEndPoint(endpoint);
    let headers = null;
    if (this.auth != null) {
       headers = new HttpHeaders().set('Authorization', `Bearer ${this.auth.token}`);
    }
    return this.http.post(wellFormedEndpoint, body, {headers});
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    let wellFormedEndpoint = this.formatEndPoint(endpoint)

    return this.http.put(wellFormedEndpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    let wellFormedEndpoint = this.formatEndPoint(endpoint)

    return this.http.delete(wellFormedEndpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    let wellFormedEndpoint = this.formatEndPoint(endpoint)

    return this.http.patch(wellFormedEndpoint, body, reqOpts);
  }
}
