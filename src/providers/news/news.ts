import { Injectable } from '@angular/core';
import {News} from "../../models/news";
import {Api} from "..";

@Injectable()
export class NewsProvider {

  constructor(private api: Api) {
  }

  async submitNews(news: News) {
    let response = await this.api.post("/news/add", news).toPromise();
    console.log(response);
    return response;
  }

  async getAllNews() {
    return await this.api.get("/news/listMyNews").toPromise();
  }


}
