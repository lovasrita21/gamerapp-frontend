import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';

import { FirstRunPage } from '../pages';
import {Api, Settings} from '../providers';
import {LocalNotifications} from "@ionic-native/local-notifications";
import {TrainingProvider} from "../providers/training/training";
import {Training} from "../models/training";

@Component({
  template: `<ion-menu [content]="content">
    <ion-header>
      <ion-toolbar>
        <ion-title>Pages</ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
          {{p.title}}
        </button>
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  rootPage = FirstRunPage;

  prevTraining: Array<Training> = [];
  allTraining: Array<Training> | any;
  trainings: Array<Training>;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Hirfolyam', component: 'NewsfeedPage' },
    { title: 'Edzések', component: 'TrainingPage' },
    { title: 'Versenyek', component: 'CompetitionEntryPage' },
    { title: 'Profil', component: 'MyprofilePage' },
  ]

  constructor( private translate: TranslateService, platform: Platform, settings: Settings,
               private config: Config, private statusBar: StatusBar,
               private splashScreen: SplashScreen, private localNotifications: LocalNotifications,
               private trainingProvider: TrainingProvider,
              private api: Api) {
    platform.ready().then(() => {
      this.trainingsChangedNotification();
      this.localNotifications.on('click').subscribe(notification => {
        this.nav.push('TrainingPage');
      });
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.initTranslate();
  }


  trainingsChangedNotification() {
    let notification: any = {
      id:0,
      text: 'Új edzésed van haver! Lesd meg!',
      led: 'FF0000',
    };

    setInterval(() => {
      if (this.api.auth.token !== '') {
        if (this.trainingsChanged()) {
          this.localNotifications.schedule(notification);
          this.localNotifications.isScheduled (0);
        } else {
        }
      }
    }, 10000);
  }
  trainingsChanged(): boolean {
    this.getTrainings();
    if (this.prevTraining !== [] && this.prevTraining !== this.trainings) {
      this.prevTraining = [];
      this.prevTraining = this.trainings;
      return true;
    } else {
      return false;
    }
  }

  async getTrainings() {
    try {
      this.allTraining = await this.trainingProvider.getMyTrainings();
      this.trainings = this.allTraining;
    } catch (e) {
      console.log(e);
    }
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }


}
